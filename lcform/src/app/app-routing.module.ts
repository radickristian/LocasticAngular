import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormMainComponent } from './form-main/form-main.component';

const routes: Routes = [
  {
    path: '',
    component: FormMainComponent,
    pathMatch: 'full' 
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class RoutingModule { }